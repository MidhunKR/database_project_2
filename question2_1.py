import sqlite3 as sq
conn=sq.connect('empl233_org.db') #a different db file has been created this time.
c=conn.cursor()
c.execute("""CREATE TABLE Department(Name text,Id int)""")
c.execute("""CREATE TABLE Employee(Name text,Id int,Salary integer,Dept_Id int)""")
conn.commit()
def add_city():
    c.execute("""ALTER TABLE Employee ADD City text""")
    conn.commit()
    print('City Column Added')
def retrieve():
    c.execute("SELECT Name,Id,Salary FROM Employee")
    i=c.fetchall()
    for j in i:
        print(j)
def dis_all():
    c.execute("SELECT * FROM Employee")
    i=c.fetchall()
    for j in i:
        print(j)
def p_match():
    print('Enter Pattern')
    n=str(input())
    c.execute("SELECT * FROM Employee WHERE Name LIKE '{}%'".format(n))
    i=c.fetchall()
    for j in i:
        print(j)
def dis_id():
    print('Enter the Id of the Employee')
    n=int(input())
    c.execute("SELECT * FROM Employee WHERE Id=?",(n,))
    i=c.fetchall()
    for j in i:
        print(j)
def change_id():
    print('Enter the Id of the Employee')
    n=int(input())
    print('Enter his NEW Name')
    s=str(input())
    c.execute("UPDATE Employee SET Name=? WHERE Id=?",(s,n))
    conn.commit()
    print('Name Updated Succesfully')
    c.execute("SELECT * FROM Employee WHERE Id=?",(n,))
    i=c.fetchall()
    for j in i:
        print(j)
    conn.commit()
def insert():
    print('Enter the name of the employee')
    emp_n=str(input())
    print('Enter the Id of the User')
    emp_id=int(input())
    print('Enter thr Salary of the User')
    emp_s=int(input())
    print('Enter Department Id')
    emp_d=int(input())
    print('Has City Column Has Been Added??   Y/N')
    jk=input()
    if jk=='Y' or jk=='y':
        print('Enter City  Name')
        emp_c=str(input())
        c.execute("INSERT INTO Employee VALUES(?,?,?,?,?)",(emp_n,emp_id,emp_s,emp_d,emp_c))
        conn.commit()
    else:
        c.execute("INSERT INTO Employee VALUES(?,?,?,?)",(emp_n,emp_id,emp_s,emp_d))
        conn.commit()



def add_dep():
    print('Enter the Name Of the Department')
    d_name=str(input())
    print('Enter the Id of the Department')
    d_id=int(input())
    c.execute("INSERT INTO Department VALUES(?,?)",(d_name,d_id))
    conn.commit()
    
    
def join():
    print('Enter The Id of Department')
    n=int(input())
    c.execute("SELECT d.Id,e.Id,e.Name,d.Name FROM Employee AS e,Department AS d ON d.Id=e.Dept_Id AND d.Id=?",(n,))
    d=c.fetchall()
    for i in d:
        print(i)

while True:
    print('\n')
    print('1.Employee menu')
    print('2.Department and Join Menu')
    n=int(input())
    if n==1:
        print('\n')
        print('Enter Your Option')
        print('1.Insert')
        print('2.Add city')
        print('3.Display Details')
        print('4.Select employees by Pattern')
        print('5.Display Details By ID')
        print('6.Change Name')
        print('7.Display All')
        n=int(input())
        if n==1:
            insert()
        if n==2:
            add_city()
        if n==3:
            retrieve()
        if n==4:
            p_match()
        if n==5:
            dis_id()
        if n==6:
            change_id()
        if n==7:
            dis_all()
    if n==2:
        print('\n')
        print('1.Add Department')
        print('2.View Department And Employees')
        n=int(input())
        if n==1:
            add_dep()

        if n==2:
            join()
            
